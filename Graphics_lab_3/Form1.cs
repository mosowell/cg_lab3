﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Graphics_lab_3
{
    public partial class Form1 : Form
    {
        Point one = new Point(); Point two = new Point();
        bool flag = false;
        List<Point> points = new List<Point>();
        Bitmap bitmap;
        public Form1()
        {
            InitializeComponent();
            bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
        }
        void drawLine(int x1, int y1, int x2, int y2, Color color)
        {
            Graphics g = Graphics.FromImage(bitmap);
            Pen redBrush = new Pen(new SolidBrush(color), 2);
            // Длина отрезка по X
            int deltaX = Math.Abs(x2 - x1);
            // Длина отрезка по Y
            int deltaY = Math.Abs(y2 - y1);
            int signX = x1 < x2 ? 1 : -1;
            int signY = y1 < y2 ? 1 : -1;
            int error = deltaX - deltaY;

            g.DrawRectangle(redBrush, x2, y2, 1, 1);

            while (x1 != x2 || y1 != y2)
            {
                g.DrawRectangle(redBrush, x1, y1, 1, 1);
                int error2 = error * 2;

                if (error2 > -deltaY)
                {
                    error -= deltaY;
                    x1 += signX;
                }
                if (error2 < deltaX)
                {
                    error += deltaX;
                    y1 += signY;
                }
            }

            pictureBox1.Image = bitmap;
        }

        void DrawRectangle(Point one, Point two)
        {
            Graphics g = Graphics.FromImage(bitmap);
            drawLine(one.X, one.Y, two.X, one.Y, Color.Black);
            drawLine(two.X, one.Y, two.X, two.Y, Color.Black);
            drawLine(two.X, two.Y, one.X, two.Y, Color.Black);
            drawLine(one.X, two.Y, one.X, one.Y, Color.Black);

            pictureBox1.Image = bitmap;
        }

        int OutCode(double x, double y, int X1, int Y1, int X2, int Y2)
        {
            // Внутри
            int code = 0;
            // Слева
            if (x < X1) code |= 0x01;
            // Сверху
            if (y < Y1) code |= 0x02;
            // Справа
            if (x > X2) code |= 0x04;
            // Снизу
            if (y > Y2) code |= 0x08;
            return code;
        }
        void Swap<T>(ref T a, ref T b)
        {
            T c = a;
            a = b;
            b = c;
        }
        void DrowInside(double x1, double y1, double x2, double y2, int X1, int Y1, int X2, int Y2)
        {
            int code1 = OutCode(x1, y1, X1, Y1, X2, Y2);
            int code2 = OutCode(x2, y2, X1, Y1, X2, Y2);
            // Оба конца внутри
            bool inside = (code1 | code2) == 0;
            // Оба конца снаружи
            bool outside = (code1 & code2) != 0;
            // Если не снаружи
            if (!outside)
            {
                // Пока не внутри и не снаружи
                while (!outside && !inside)
                {
                    // Если первая точка внутри
                    if (code1 == 0)
                    {
                        Swap(ref x1, ref x2);
                        Swap(ref y1, ref y2);
                        Swap(ref code1, ref code2);
                    }
                    byte res = (byte)code1;
                    // Слева
                    if ((res & 0x01) == 0x01)
                    {
                        y1 += (y2 - y1) * (X1 - x1) / (x2 - x1);
                        x1 = X1;
                    }
                    // Сверху
                    if ((res & 0x02) == 0x02)
                    {
                        x1 += (x2 - x1) * (Y1 - y1) / (y2 - y1);
                        y1 = Y1;
                    }
                    // Справа
                    if ((res & 0x04) == 0x04)
                    {
                        y1 += (y2 - y1) * (X2 - x1) / (x2 - x1);
                        x1 = X2;
                    }
                    // Снизу
                    if ((res & 0x08) == 0x08)
                    {
                        x1 += (x2 - x1) * (Y2 - y1) / (y2 - y1);
                        y1 = Y2;
                    }

                    code1 = OutCode(x1, y1, X1, Y1, X2, Y2);
                    code2 = OutCode(x2, y2, X1, Y1, X2, Y2);
                    inside = (code1 | code2) == 0;
                    outside = (code1 & code2) != 0;
                }
                if (inside)
                {
                    drawLine((int)Math.Round(x1), (int)Math.Round(y1), (int)Math.Round(x2), (int)Math.Round(y2), Color.Blue);
                }
            }
        }

        void RefreshLines() 
        {
            for (int i = 0; i < points.Count - 1; i++)
            {
                drawLine(points[i].X, points[i].Y, points[i + 1].X, points[i + 1].Y, Color.Red);
            }
        }

        private void pictureBox1_DownClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                one = new Point(e.X, e.Y);
            }
            if (e.Button == MouseButtons.Left)
            {
                Graphics g = Graphics.FromImage(bitmap);
                points.Add(new Point(e.X, e.Y));
                if (points.Count == 1)
                {
                    Pen blackBrush = new Pen(new SolidBrush(Color.Black), 2f);
                    g.DrawRectangle(blackBrush, e.X, e.Y, 1, 1);
                }
                else
                {
                    Point prev = points[points.Count - 2];
                    drawLine(prev.X, prev.Y, e.X, e.Y, Color.Red);
                }
                pictureBox1.Image = bitmap;
            }
        }
        private void pictureBox1_UpClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Graphics g = Graphics.FromImage(bitmap);
                if (points.Count > 0)
                {
                    if (!flag)
                    {
                        flag = true;
                        two = new Point(e.X, e.Y);
                        DrawRectangle(one, two);
                        int X1 = one.X;
                        int Y1 = one.Y;
                        int X2 = two.X;
                        int Y2 = two.Y;
                        if (one.X > two.X) Swap(ref X1, ref X2);
                        if (Y1 > Y2) Swap(ref Y1, ref Y2);
                        RefreshLines();
                        // Проходим по всем точкам ломаной
                        for (int i = 0; i < points.Count - 1; i++)
                        {
                            // Рисуем линию внутри прямоугольника другим цветом
                            DrowInside(points[i].X, points[i].Y, points[i + 1].X, points[i + 1].Y, X1, Y1, X2, Y2);
                        }
                    }
                    // Если есть рисунок
                    else
                    {
                        g.Clear(Color.White);
                        flag = false;
                        pictureBox1_UpClick(sender, e);
                    }
                    pictureBox1.Image = bitmap;
                }
            }
        }
    }
}
